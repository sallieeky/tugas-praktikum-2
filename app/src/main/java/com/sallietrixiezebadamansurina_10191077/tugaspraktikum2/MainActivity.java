package com.sallietrixiezebadamansurina_10191077.tugaspraktikum2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.sallietrixiezebadamansurina_10191077.tugaspraktikum2.activity.FragmentActivity;

public class MainActivity extends AppCompatActivity {

    ImageView iv;
    Button btn, btn_pindah;
    boolean status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iv = findViewById(R.id.iv);
        btn = findViewById(R.id.btn);
        btn_pindah = findViewById(R.id.btn_pindah);
        status = false;

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOff();
            }
        });

        btn_pindah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FragmentActivity.class);
                startActivity(intent);
            }
        });
    }

    public void onOff() {
        if(!status) {
           iv.setImageResource(R.drawable.on);
           status = true;
        } else {
            iv.setImageResource(R.drawable.off);
            status = false;
        }
    }

}