package com.sallietrixiezebadamansurina_10191077.tugaspraktikum2.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sallietrixiezebadamansurina_10191077.tugaspraktikum2.R;
import com.sallietrixiezebadamansurina_10191077.tugaspraktikum2.fragment.HomeFragment;
import com.sallietrixiezebadamansurina_10191077.tugaspraktikum2.fragment.StatusFragment;

public class FragmentActivity extends AppCompatActivity {

    RelativeLayout rl;
    TextView tv_home, tv_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        tv_home = findViewById(R.id.tv_home);
        tv_status = findViewById(R.id.tv_status);

        tv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.rl, new HomeFragment());
                ft.commit();
            }
        });

        tv_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.rl, new StatusFragment());
                ft.commit();
            }
        });
    }
}