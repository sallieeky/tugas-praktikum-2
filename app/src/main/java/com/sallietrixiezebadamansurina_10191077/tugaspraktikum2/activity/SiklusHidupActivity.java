package com.sallietrixiezebadamansurina_10191077.tugaspraktikum2.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import com.sallietrixiezebadamansurina_10191077.tugaspraktikum2.R;

public class SiklusHidupActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_siklus_hidup);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "Method onStart", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this, "Method onStop", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, "Method onPause", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "Method onResume", Toast.LENGTH_SHORT).show();
    }
}